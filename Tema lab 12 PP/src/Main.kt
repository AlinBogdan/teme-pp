import java.io.File
import kotlin.reflect.typeOf

fun main()
{
    //ex1
    var list1 = listOf<Int>(1, 21, 75, 39, 7, 2, 35, 3, 31, 7, 8)
    list1 = list1.filter{it >= 5} //21, 75, 39, 7, 35, 31, 7, 8
    val list2 = list1.zipWithNext().filter{pair: Pair<Int, Int> -> list1.zipWithNext().indexOf(pair) % 2 == 0}
    //(21, 75), (39, 7), (35, 31), (7, 8)
    val list3 = list2.map{pair: Pair<Int, Int> -> (pair.first * pair.second)}
    //1575, 273, 1085, 56
    val list4 = list3.fold(0){i, j -> i + j}
    println(list4)


    //ex 2
    val fin = File("input.txt").readText().split(' ').toList().onEach{it.toCharArray(); if(it.length in 4..7) it.onEach{char: Char -> char.inc()}} //pseudo-caesar cu shift +1
    println(fin)

}