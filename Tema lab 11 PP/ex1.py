import asyncio
import queue

q = queue.Queue()
q.put(10)
q.put(20)
q.put(30)
q.put(40)


@asyncio.coroutine
def add_fun1(n, future):
    count = 0
    for i in range(1, n+1):
        count += i
    yield from asyncio.sleep(3)
    future.set_result(f'result = {count}')


@asyncio.coroutine
def add_fun2(n, future):
    count = 0
    for i in range(1, n+1):
        count += i
    yield from asyncio.sleep(3)
    future.set_result(f'result = {count}')


@asyncio.coroutine
def add_fun3(n, future):
    count = 0
    for i in range(1, n+1):
        count += i
    yield from asyncio.sleep(3)
    future.set_result(f'result = {count}')


@asyncio.coroutine
def add_fun4(n, future):
    count = 0
    for i in range(1, n+1):
        count += i
    yield from asyncio.sleep(3)
    future.set_result(f'result = {count}')


def print_result(future):
    print(future.result())


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    my_future1 = asyncio.Future()
    my_future2 = asyncio.Future()
    my_future3 = asyncio.Future()
    my_future4 = asyncio.Future()
    tasks = [add_fun1(q.get(), my_future1), add_fun2(q.get(), my_future2), add_fun3(q.get(), my_future3), add_fun4(q.get(), my_future4)]
    my_future1.add_done_callback(print_result)
    my_future2.add_done_callback(print_result)
    my_future3.add_done_callback(print_result)
    my_future4.add_done_callback(print_result)

    loop.run_until_complete(asyncio.wait(tasks))
    loop.close()
