class AND4(var input: ArrayList<Int>) : ANDGate {

    override fun getResult(): Int {
         var result : Int = input[0]

        input.forEach {
            result = result and it
        }
        return result
    }
}