class ANDGateSelector() {
    lateinit var andGate: ANDGate

    fun selectGate(input: ArrayList<Int>){
        when(input.size) {
            2 -> andGate = AND2(input)
            4 -> andGate = AND4(input)
            8 -> andGate = AND8(input)
            else -> print("Numar de intrari incorect")
        }
    }

    fun getGate() : ANDGate {
        return andGate
    }
}