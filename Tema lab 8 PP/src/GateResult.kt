class GateResult(var gate: ANDGateSelector) {
    fun result(input: ArrayList<Int>) : Int {
        gate.selectGate(input)

        return gate.getGate().getResult()
    }

}