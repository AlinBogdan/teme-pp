class AND2(var input: ArrayList<Int>) : ANDGate{
    override fun getResult(): Int {
        var result = input[0]
        input.forEach{
            result = result and it
        }
        return result
    }
}